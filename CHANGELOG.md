Changelog
=========

1.1.1 (2017-06-21)
------------------

Fix
~~~

- Process now returns failed if any error occured during processing, not
  just when it occured in the last branch of a tree. [benedict]

1.1.0 (2017-05-02)
------------------

Changes
~~~~~~~

- The runtime can now be queried if it is currently processing a graph.
  [benedict]

Other
~~~~~

- Removed repositories from project.clj Updated readme. [benedict]

- Taged as 1.0 Generalised repository urls. [benedict]

- Fix dispatching. [e-k-m]

- Pump to new archiva repo. [e-k-m]

- Add support for filter components. [e-k-m]

- Update README. [e-k-m]

- Initial commit. [e-k-m]


