# graphite

> embedded event processing with components

[TOC]

A Clojure library for embedded event processing. Combining a simple
event handling system and processing definitions by linked components
in order to perform evented processing of payloads.

graphite is heavily influenced by Clojurewerkz's
[EEP](https://github.com/clojurewerkz/eep) library.

## Project Maturity

Graphite is in productive use by commercial applications. The features are basic, but stable and reliable.
It seriously needs to be updated to use current Reactor versions, though.

## Maven Artifacts

### Most Recent Release

With Leiningen:

	[graphite "1.1.0"]

With Maven:

	<dependency>
	  <artifactId>graphite</artifactId>
	  <version>1.0</version>
	</dependency>


## Quick-start

Let's build a simple calculator that adds a number to a incoming number
and finally prints the result to stdout:

Fire up a REPL and start off by requiring the needed namespaces:

```clojure
(ns user
  (:require [graphite.runtime :as runtime]
			[graphite.graph :as graph]))
```

Define `number` as source-, `adder` as transformer- and `number` as
the sink-component to be used in the processing graph:

```clojure
(defn number [{:keys [n]}]
  (let [exec (fn [{:keys [handler]}] (handler n))]
	{:exec exec
	 :type :source}))

(defn adder [{:keys [n]}]
  (let [exec (fn [p] (+ p n))]
	{:exec exec
	 :type :transformer}))

(defn printer []
  (let [exec (fn [p] (println p))]
	{:exec exec
	:type :sink}))
```

Define the processing graph; - essentially the definition of the
application logic:

```clojure
(def graph (graph/defgraph [[:number number {:n 1}] -> [:adder adder {:n 2}]
							[:adder] -> [:printer printer]]))
```

Create and start the runtime:

```clojure
(def r (-> (runtime/create graph) (runtime/start)))
```

If you need to react to errors while processing, or simply want to wait until the graph finishes processing,
 you can watch the state of the runtime:
 
 ```clojure
 (while (= :active (runtime/get-status runtime) (pick-nose)))     ;wait until the runtime finishes processesing its graph.
           (when (= :failed (runtime/get-status runtime))
             (run-around-in-panic))           ;handle failed processing appropriately.
 ```

If everything went as expected this should print the number 3 (= 1 +
2).

Finally stop the runtime:

```clj
(runtime/stop r)
```

## Building and deployment

Define build repositories as local profiles, whether project (create profiles.clj in project folder), 
user (add profiles to ~/.lein/profiles.clj) or system (add profiles to /etc/leiningen/profiles.clj) specific.

```clojure

;;;; PROFILE EXAMPLE

{:graphite-debug {:repositories [["snapshots" {:url "<url-of-snapshot-repository>"
                                               :username "<repo-user>" 
                                               :password "<repo-password>"}]]}

 :graphite-release {:repositories [["internal" {:url "<url-of-release-repository>"
                                                :username "<repo-user>" 
                                                :password "<repo-password>"}]]}}
```
Then build with e.g.

```bash
lein with-profile graphite-debug deploy snapshots
```

## Documentation

### Core Concepts

- 'Component': A component defines processing logic to be a applied to
a payload coming from a single previous node and going to multiple
following nodes.

- 'Node': A node refers to a 'instance' of a component.

- 'Processing Graph': A collection of nodes and edges / connections
  between them.

- 'Runtime': Takes a processing graph and makes the magic happen.

### Components

Components are factory functions with keyword arguments that return a
map with the mandatory fields `:exec` and `:type`. For components of
type `:transformer` and `:sink` the value of `:exec` must be a
function that takes the incoming payload from the previous component
as a single argument and returns the payload for the next
component. On the other hand the `:exec` function of source
components, hence of type `:source`, must have the signature `(fn
[{:keys [handler]}] body)` and use the handler to send payloads to the
next component.

Optionally the returned map can have a field `:cleanup` referring to
an zero argument function, which will be called on
`graphite.runtime/stop` and should perform any clean up tasks needed
(e.g. close a database connection the factory function created).

### Processing Graph Definition

Graphite has its own embedded Domain-Specific Language (DSL) for easy
graph definition. The syntax is the following:

- `[:a component-a] -> [:b component-b {:args xy}]` sets up a
connection between node :a and :b using the corresponding components
and arguments within a map.

Using the DSL and `graphite.graph/defgraph` a processing graph can be
defined as follows:

```clj
(def g (graphite.graph/defgraph [[...] -> [...] ...]))
```

Alternatively the graph can be defined using `graphite.graph/new-graph`,
`graphite.graph/add-component` and `graphite.graph/add-edge` as follows:

```clj
(def g (-> (graph/new-graph)
		   (graph/add-component :node-a component {:arg x})
		   (graph/add-component :node-b component)
		   (graph/add-edge :node-a :node-b)))
```

### Runtime

The heart of graphite is the runtime. Given a processing graph
definition `graphite.graph/create` returns a new runtime. Which then
can be started and stopped using `graphite.graph/start` and
`graphite.graph/stop`, respectively.

`graphite.graph/create` can optionally be called with the keyword
argument `:dispatcher-type`.

Thereby dispatchers provide you a toolkit for both threadpool-style
long-running execution to high-throughput task dispatching, with:

- default one, synchronous dispatcher, implementation that dispatches
events using the calling thread.

- `:event-loop` dispatcher implementation that dispatches events using
the single dedicated thread. Together with default synchronous
dispatcher, very useful in development mode.

- `:thread-pool` dispatcher implementation that uses
ThreadPoolExecution with an unbounded queue to dispatch events. Works
best for long-running tasks.

- `:ring-buffer` dispatcher implementation that uses LMAX Disruptor
RingBuffer to queue tasks to execute. Known to be most high-throughput
implementation.

### Exception Handling

Whenever an Exception occurs inside of your processing graph,
downstream components won't receive any payloads. In order to debug
your processing graph and see what exactly happened, you should
subscribe to the `Exception` events by using `on-error`.

Most generic case would be:

```clj
(graphite.runtime/on-error r Exception (fn [event]
										 (println event)))
```

So whenever any Exception (of any type) occurs inside your processing
graph, your handler will be executed. You can also use sub-classes:

```clj
(graphite.runtime/on-error r RuntimeException (fn [event]
												(println event)))
```

In this case your handler will be only triggered when type of an
occurred exception is `RuntimeException` or one of the derived
classes.

You can have more than one handler per `Exception` type.


### Flow Debugging

To debug the 'flow' of the processing graph, one can add a handler to
the runtime as follows:

```clj
(runtime/on-flow r (fn [event] (print event)))
```

## License

Copyright © 2015 IVGI, FHNW

Distributed under the [Eclipse Public License](http://www.eclipse.org/legal/epl-v10.html)
either version 1.0 or (at your option) any later version.
