(ns graphite.graph-test
  (:require [clojure.test :refer :all]
            [graphite.graph :refer :all]))

(deftest test-create-graph
  (is (= (new-graph) {:d {} :p {}})))

(deftest test-add-node
  (let [n "node-1"]
    (is (= (add-node (new-graph) n) {:d {n []} :p {}}))))

(deftest test-remove-node-source
  (let [n "node-1"
        g {:d {n []} :p {}}]
    (is (= (remove-node-source g n) {:d {} :p {}}))))

(deftest test-remove-node-destination
  (let [n1 "node-1"
        n2 "node-2"
        g {:d {n1 [n2]} :p {}}]
    (is (= (remove-node-destination g n2) {:d {n1 []} :p {}}))))

(deftest test-remove-node-destination
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        g  {:d {n1 [n2]
                n3 [n1]}
            :p {}}]
    (is (= (remove-node g n1) {:d {n3 []} :p {}}))))

(deftest test-rename-node-source
  (let [n1 "node-1"
        n2 "node-2"
        g  {:d {n1 []} :p {}}]
    (is (= (rename-node-source g n1 n2) {:d {n2 []} :p {}}))))

(deftest test-rename-node-destination
  (let [n1 "node-1"
        n2 "node-2"
        g  {:d {n1 [n1]} :p {}}]
    (is (= (rename-node-destination g n1 n2) {:d {n1 [n2]} :p {}}))))

(deftest test-rename-node
  (let [n1 "node-1"
        n2 "node-2"
        g  {:d {n1 [n1]} :p {}}]
    (is (= (rename-node g n1 n2) {:d {n2 [n2]} :p {}}))))

(deftest test-add-edge
  (let [n1 "node-1"
        n2 "node-2"
        g  {:d {n1 [] n2 []} :p {}}]
    (is (= (add-edge g n1 n2) {:d {n1 [n2] n2 []} :p {}}))))

(deftest test-remove-edge
  (let [n1 "node-1"
        n2 "node-2"
        g  {:d {n1 [n2]} :p {}}]
    (is (= (remove-edge g n1 n2) {:d {n1 []} :p {}}))))

(deftest test-add-property
  (let [n "node-1"
        g {:d {n []} :p {}}]
    (is (= (add-property g n "...") {:d {n []} :p {n "..."}}))))

(deftest test-remove-property
  (let [n "node-1"
        g {:d {n []} :p {n "..."}}]
    (is (= (remove-property g n) {:d {n []} :p {}}))))

(deftest test-add-component
  (let [n         "node-1"
        component nil
        config    nil
        g         {:d {} :p {}}]
    (is (= (add-component g n component config)
           {:d {n []} :p {n {:component component :config config}}}))))

(deftest test-remove-component
  (let [n         "node-1"
        component nil
        config    nil
        g         {:d {n []} :p {n {:component component :config config}}}]
    (is (= (remove-component g n) {:d {} :p {}}))))

(deftest test-get-nodes
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        g  {:d {n1 [] n2 [] n3 []}}]
    (assert (= (get-nodes g) [n1 n2 n3]))))

(deftest test-get-outgoing
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        g  {:d {n1 [n2 n3]} :p {}}]
    (assert (= (get-outgoing g n1) [n2 n3]))))

(deftest test-get-incoming
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        n4 "node-4"
        g  {:d {n1 [n4]
                n2 [n4]
                n3 []}
            :p {}}]
    (assert (= (get-incoming g n4) [n1 n2]))))

(deftest test-get-spouts
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        n4 "node-4"
        g  {:d {n1 [n2]
                n2 [n3]
                n3 [n4]}
            :p {}}]
    (assert (= (get-spouts g) [n1]))))

(deftest test-get-sinks
  (let [n1 "node-1"
        n2 "node-2"
        n3 "node-3"
        n4 "node-4"
        g  {:d {n1 [n2]
                n2 [n3 n4]
                n3 []
                n4 []}
            :p {}}]
    (assert (= (get-sinks g) [n3 n4]))))

(deftest test-defgraph
  (let [g {:d {:a [:b]
               :b [:c]
               :c []}
           :p {:a {:component :ca :config nil }
               :b {:component :cb :config nil }
               :c {:component :cc :config nil }}}]
    (assert (= (defgraph
                 [[:a :ca] -> [:b :cb]
                  [:b] -> [:c :cc]])
               g))))
