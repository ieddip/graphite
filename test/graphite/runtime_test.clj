(ns graphite.runtime-test
  (:require [clojure.test :refer :all]
            [graphite.graph :as graph]
            [graphite.runtime :as runtime]))

;;; helpers

(def sleepy-time 1000)

(def data-to-send [1 2 3 4 5 6 7])

(defn sender []
  (let [exec (fn [{:keys [handler]}]
               (doseq [n data-to-send]
                 (handler n)))]
    {:exec exec	:type :source}))

(defn delayer []
  (let [exec (fn [p]
               (Thread/sleep (rand-int sleepy-time))
               p)]
    {:exec exec	:type :transformer}))

(defn collector [{:keys [data-received]}]
  (let [exec (fn [p]
               (swap! data-received conj p))]
    {:exec exec	:type :sink}))

;;; dispatcher tests

(deftest test-default-dispatcher
  (let [data-received (atom [])
        graph (graph/defgraph
                [[:sender sender] -> [:delayer delayer]
                 [:delayer] -> [:collector collector {:data-received data-received}]])
        r (runtime/create graph)
        x (promise)]
    (add-watch data-received :w (fn [k a os ns]
                                  (when (= (count @a) (count data-to-send))
                                    (deliver x @a))))
    (runtime/start r)
    (is (= @x data-to-send))
    (runtime/stop r)))

(deftest test-event-loop-dispatcher
  (let [data-received (atom [])
        graph (graph/defgraph
                [[:sender sender] -> [:delayer delayer]
                 [:delayer] -> [:collector collector {:data-received data-received}]])
        r (runtime/create graph :dispatcher-type :event-loop)
        x (promise)]
    (add-watch data-received :w (fn [k a os ns]
                                  (when (= (count @a) (count data-to-send))
                                    (deliver x @a))))
    (runtime/start r)
    (is (= @x data-to-send))
    (runtime/stop r)))

(deftest test-thread-pool-dispatcher
  (let [data-received (atom [])
        graph (graph/defgraph
                [[:sender sender] -> [:delayer delayer]
                 [:delayer] -> [:collector collector {:data-received data-received}]])
        r (runtime/create graph :dispatcher-type :thread-pool)
        x (promise)]
    (add-watch data-received :w (fn [k a os ns]
                                  (when (= (count @a) (count data-to-send))
                                    (deliver x @a))))
    (runtime/start r)
    (is (not= @x data-to-send))
    (is (= (set @x) (set data-to-send)))
    (runtime/stop r)))

(deftest test-ring-buffer-dispatcher
  (let [data-received (atom [])
        graph (graph/defgraph
                [[:sender sender] -> [:delayer delayer]
                 [:delayer] -> [:collector collector {:data-received data-received}]])
        r (runtime/create graph :dispatcher-type :ring-buffer)
        x (promise)]
    (add-watch data-received :w (fn [k a os ns]
                                  (when (= (count @a) (count data-to-send))
                                    (deliver x @a))))
    (runtime/start r)
    (is (= @x data-to-send))
    (runtime/stop r)))

;;; routing tests

(deftest test-split-routing
  (let [data-received-1 (atom [])
        data-received-2 (atom [])
        graph (graph/defgraph
                [[:sender sender] -> [:delayer delayer]
                 [:delayer] -> [:collector-1 collector {:data-received data-received-1}]
                 [:delayer] -> [:collector-2 collector {:data-received data-received-2}]])
        r (runtime/create graph)
        x (promise)
        y (promise)]
    (add-watch data-received-1 :w (fn [k a os ns]
                                    (when (= (count @a) (count data-to-send))
                                      (deliver x @a))))
    (add-watch data-received-2 :w (fn [k a os ns]
                                    (when (= (count @a) (count data-to-send))
                                      (deliver y @a))))
    (runtime/start r)
    (is (= @x data-to-send))
    (is (= @y data-to-send))
    (runtime/stop r)))
