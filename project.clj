(defproject graphite "1.1.3-SNAPSHOT"
  :description "graphite, embedded event processing with components"
  :url "https://bitbucket.org/ieddip/graphite"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clojurewerkz/meltdown "1.1.0"]])
