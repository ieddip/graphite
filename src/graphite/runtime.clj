(ns graphite.runtime
  (:require [graphite.foreman :as foreman]
            [graphite.graph :as graph]
            [clojure.set :as set]))

;;;; ----------------------     --------------------------------------
;;;; --------------------   ``` --------------------------------------
;;;; -----------------  ````-+o+. ------------------------------------
;;;; --------------  `.-//::-:+++. -----------------------------------
;;;; ----------   ..-:::://:--:/+o. ----------------------------------
;;;; ----------- :so:-:::://:::::-:. ---------------------------------
;;;; -----------  +so:-------..sh-.:. --------------------------------
;;;; ------------- /ys-``.----.-:--//- -------------------------------
;;;; -------------- :s+.``..----:+oshh. ------------------------------
;;;; --------------- -o+-``.--/+o/:-/- -------------------------------
;;;; ---------------- .o+-``./s::: -----------------------------------
;;;; ----------------- .oo:.  /+:-:` ---------------------------------
;;;; ------------------ `-.    :/:-:` --------------------------------
;;;; -------------------------  :/:.:. -------------------------------
;;;; --------------------------- :/:-:. ------------------------------
;;;; ---------------------------- ://-:-` ----------------------------
;;;; ----------------------------- ///-::. ---------------------------
;;;; -----------------------------  +:/---::-` -----------------------
;;;; -----------------------------  .+::-:-/+/`  ---------------------
;;;; -----------------------------  :::-.   +y/` ---------------------
;;;; -----------------------------  :/:-    .yo/`  -------------------
;;;; -----------------------------   /+:.   :s+/` --------------------
;;;; -----------------------------    :+/--+o+:. ---------------------
;;;; ------------------------------    -+////-` ----------------------
;;;; ------------------------------      ```  ------------------------

;;;; HACK: Add tests. (mae 15-10-22)
;;;;
;;;; TODO: Maybe add notify. (mae 15-10-30)
;;;;
;;;; HACK: Creating a verbose component should be optional, since this
;;;;       puts extra load on the event handler. (mae 15-10-30)

;;; Component stuff

(defn verbose-component
  [r n c]
  (update-in c [:exec] (fn [f]
                         (fn [p]
                           (foreman/notify (:foreman r) :flow {:node n :payload p})
                           (f p)))))

(defn setup-component [m]
  (if-let [config (:config m)]
    ((:component m) config)
    ((:component m))))

(defn setup-components [r]
  (let [graph (:graph r)
        nodes (graph/get-nodes graph)
        properties (graph/gp graph)]
    (assoc r
           :components
           (reduce #(assoc %1 %2 (->> (setup-component (get properties %2))
                                      (verbose-component r %2)))
                   {}
                   nodes))))

;;; Helpers of Big George

(def sources (atom {}))

(defn source
  [foreman n f ts]
  (->> (fn [] (f {:handler (foreman/handler foreman ts)}))
       (swap! sources assoc n))
  foreman)

;;; Connecting components

(defmulti connect
  (fn [r n c] (:type c)))

(defmethod connect :source
  [r n c]
  (source (:foreman r) n (:exec c) (graph/get-outgoing (:graph r) n)))

(defmethod connect :sink
  [r n c]
  (foreman/sink (:foreman r) n (:exec c)))

(defmethod connect :transformer
  [r n c]
  (foreman/transformer (:foreman r) n (:exec c) (graph/get-outgoing (:graph r) n)))

(defmethod connect :filter
  [r n c]
  (foreman/filterer (:foreman r) n (:exec c) (graph/get-outgoing (:graph r) n)))

(defn connect-components
  [r]
  (doseq [[n c] (:components r)]
    (connect r n c))
  r)

;;; Core

(defn new-runtime [g dispatcher-type]
  {:graph g
   :foreman (foreman/create {:dispatcher-type dispatcher-type})})

(defn create
  [g & {:keys [dispatcher-type]}]
  (-> (new-runtime g dispatcher-type) setup-components connect-components))

(defn start [r]
  (doseq [f (vals @sources)]
    (f))
  r)

(defn cleanup [r]
  (doseq [n (keys @sources)]
    (if-let [f (:cleanup (get (:components r) n))]
      (f)))
  r)

(defn shutdown [r]
  (foreman/stop (:foreman r))
  r)

(defn stop [r]
  (-> r cleanup shutdown))

(defn get-status
  "Returns the current status of the runtime.
  Possible values: :failed, :idle, :active."
  [runtime]
  (foreman/get-status (:foreman runtime)))

(defn on-error
  [r e f]
  (foreman/on-error (:foreman r) e f)
  r)

(defn on-flow
  [r f]
  (foreman/add-handler (:foreman r) :flow f)
  r)
