(ns graphite.foreman
  (:require clojure.pprint
            [clojurewerkz.meltdown.reactor :as mr]
            [clojurewerkz.meltdown.selectors :as ms :refer [$]])
  (:import [java.util.concurrent Executors]))

;;;; -----------------:-:://///::-------------------------------------
;;;; -----------------/ohhhhhhhhhso:----------------------------------
;;;; ----------------ohddddddddddddhs:--------------------------------
;;;; --------------:yddddmdmddmddddddh:-------------------------------
;;;; --------------yddmmmmmmmmmmddmmddo-------------------------------
;;;; --------------ydddmmmmmmmmdyhmmmdh-------------------------------
;;;; --------------:sdhhhhyyyysossdmmd+-------------------------------
;;;; ---------------:odhsoo//+osyyymho:-------------------------------
;;;; ----------------:ysyoo+ooossyyhshso+/::--------------------------
;;;; ----------------:ssooo/o+++osydhhsoooooo++/::--------------------
;;;; --------------/syhhsso:/oooshddhyo///+oooosss+/::----------------
;;;; -------------/hhyysyss++ooyhddyys+///+osoosyyyys+/:--------------
;;;; -------------shyso+ohysoyhdmdhhsooo+//ysssssoosyhhs:-------------
;;;; ------------:yyssssyyhyyhdhhhhysso+/+oyssso++++syyhs:------------
;;;; -------------sysyyyyyyyyssosyssssssooosssso+///osyhd/------------
;;;; -------------/yyshhyssssssosyysssssoooooosoo+//+oyhdy:-----------
;;;; --------------shyshhysssssoshhyyyyhhhyysosyyso+ooshdd+--:--------
;;;; -----------:-+dmmddddhyssssshhyydmmmmmmdhhddys++oshddh:----------
;;;; ------------+mmmmdyhmmdhyyssydhhmmmddmmmmmmdyo//osyhhds:---------
;;;; -----------/mmmmdh/dmmmddhhysyddmmmmmmmmmmmmyo/:/osyhddo---------
;;;; -----------ymmmmd+smmmdymddyssyddmmddyydmmmmd++::osyhddm+--------
;;;; -----------ymmmdoymmmmm/Nmdysyyysydddhhmmmmmos+/oosshhhdh/-------
;;;; -----------:ydmdddmmmdd:mddyyhhysooyhhdmmmmmmds/ddddhhhhdy:------
;;;; -----------::yhyyyhdddm:hddhyhhyyo++ooshmdmdmmmhdhhhddhddd:------
;;;; ------------::+oyhhddho-yhdhhhdhyssoossshddddddddhhhdddddh-------
;;;; ---------------::/+++:-:yhddhdddhyssssssossyhddhhddddddddy-------
;;;; -----------------------:oyhhhhddhyysssssoossyyydhhhhhdddh/-------
;;;; -----------------------:ossso+syyysssooooooossyyyo::////:--------
;;;; ---------------------://+sy+////+++++///+/+osssss++--------------
;;;; ---------------------o/:+/o+//::/++++///+++soooo+/o:-------------
;;;; --------------------:do/+o::////o++++osssooysoo+o+s/:------------
;;;; ------------------::/ddo/s::/::os/os/yyoysyhos/ss+yy-------------
;;;; -------------------s/omh+y/:::/:/soo/:++ssosyohysodd:------------
;;;; ------------------:ho/oshs+::::-+sos/sshhossyddddmms-------------
;;;; -------------------hysssosh//oo:/ohooshsssyyhdmmmdo--:---:-------
;;;; -------------------/sosyh++ssoyydh+/+++syyys+//o+o+-:----:-------

;;;; HACK: Add tests. (mae 15-10-22)

;;; Big George

(def pool-size (-> (Runtime/getRuntime)
                   .availableProcessors
                   inc))

(defonce notify-pool (Executors/newFixedThreadPool (int pool-size)))

(defn sync-submit
  [f]
  (.submit notify-pool ^Callable f))

(defprotocol IForeman
  (add-handler [_ t handler])
  (delete-handler [_ t])
  (get-handler [_] [_ t])
  (notify [_ t data])
  (notify-in-pool [_ t data])
  (swap-handler [_ t handler] )
  (stop [_])
  (register-process-start [this])
  (register-process-end [this])
  (set-process-failed [this])
  (get-status [this])
  (alive? [_])
  (on-error [_ e f]))

(defrecord Foreman [handlers errors reactor running-processes had-errors]
  IForeman
  ;; etc
  (add-handler [this t handler]
    (when (nil? (get handlers t))
      (mr/on reactor ($ t) handler)
      (.select (.getConsumerRegistry reactor) t)
      (swap! handlers assoc t handler))
    this)

  (delete-handler [this t]
    (when-let [old-handler (get-handler this t)]
      (try
        (.unregister (.getConsumerRegistry reactor) t)
        (catch Exception e))
      (swap! handlers dissoc t)
      old-handler))

  (swap-handler [this t handler]
    (let [old-handler (delete-handler this t)]
      (add-handler this t handler)
      old-handler))

  (notify [_ t data]
    (mr/notify reactor t data))

  (notify-in-pool [_ t data]
    (sync-submit
     #(mr/notify reactor t data)))

  (get-handler [_]
    @handlers)

  (get-handler [_ t]
    (get @handlers t))

  (stop [_]
    (-> reactor
        (.getDispatcher)
        (.shutdown)))

  (register-process-start
    ;; increments the count of currently running processes by 1.
    [this]
    (if (= :active (get-status this))
      (swap! running-processes inc)       ;the runtime is already running
      (do (reset! running-processes 1)    ;The runtime just starts
          (reset! had-errors false))))

  (register-process-end
    ;; Decrements the count of running processes by 1, 10 milliseconds after the call was made (asynchronous).
    ;; The reason for this delay is that finished nodes will register before the next one starts. The delay is
    ;; usually below a millisecond, but it is unknown how this will behave with load. There would be a non-zero
    ;; chance of the activity status being polled right between the execution of two nodes.
    [this]
    (when (> @running-processes 0)
      (swap! running-processes dec)))

  (set-process-failed
    ;; Resets the amount of running processes to 0. Use in case of errors.
    [this]
    (reset! had-errors true)
    (swap! running-processes dec))

  (get-status
    ;; returns :failed if a graph encountered an error while processing,
    ;; :idle if the graph is not currently working, or :active if it is being run.
    [this]
    (if (> 0 @running-processes)
      :active
      (if @had-errors
        :failed
        :idle)))


  (alive? [_]
    (-> reactor
        (.getDispatcher)
        (.alive)))

  (on-error [_ e f]
    (mr/on-error reactor e f))

  (toString [_]
    (letfn [(pprint-to-str
                [& objs]
              (let [w (java.io.StringWriter.)]
                (clojure.pprint/pprint objs w)
                (.toString w)))]
      (pprint-to-str "\n" (mapv #(.toString %) @handlers)))))

(defn create
  ([{:keys [dispatcher-type]}]
   (let [reactor (mr/create :dispatcher-type dispatcher-type)]
     (Foreman. (atom {}) (atom []) reactor (atom 0) (atom false))))
  ([] (create {})))

;;; Helpers of Big George

(defn handler
  [foreman ts]
  (fn [data]
    (if (sequential? ts)
      (doseq [t ts]
        (notify foreman t data))
      (notify foreman ts data))))


(defn- with-process-registration
  "registers a process start, executes f, then registers a process stop.
  If an exception is thrown, processes are reset to 0 and exception is rethrown."
  [foreman f]
  (try
    (register-process-start foreman)
    (f)
    (register-process-end foreman)
    (catch Exception e
      (set-process-failed foreman)
      (throw e))))

(defn transformer
  [foreman t f ts]
  (add-handler foreman t (fn [{data :data}]
                           (with-process-registration
                             foreman
                             (fn [] ((handler foreman ts) (f data)))))))


(defn filterer
  [foreman t f ts]
  (add-handler foreman t (fn [{data :data}]
                           (when-let [r (f data)]
                             (with-process-registration
                               foreman
                               (fn [] ((handler foreman ts) r)))))))


(defn sink
  [foreman t f]
  (add-handler foreman t (fn [{data :data}]
                           (with-process-registration
                             foreman
                             (fn [] (f data))))))


