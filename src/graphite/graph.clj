(ns graphite.graph
  (:require [clojure.set :as set]))

;;;; -----------------------------------------------------------------
;;;;                                                 -.
;;;;                            :                   `:-  `
;;;;                           `+                    /` -`
;;;;                           ``.                `  -.-.       .
;;;;                           `o/                 -.--`    `. /-
;;;;                          :`y     .            .:-.   ``
;;;;      /                   ./+    :`             ./``  .- `. .-`
;;;;      y                    -+   -..       `      :`:  -.-- :/-
;;;;      y                    `+. -/:.       `..    -```
;;;;      y  ``          `:    `+/.+.          --  ` --/..
;;;;      y ``           .- . `++:             -``- -``
;;;;      y o.             -.: `o/               .-` --.
;;;;  ``  y+-              `:/ ``              `` -.
;;;;   o` d:               --+``+               `-.- -`
;;;;   .o y                 ``+                 ``
;;;;    .oy                   `/+                   ``
;;;;     -d                    .+                    :`
;;;;      y                    `+                    -`
;;;;      y                    `+                    -`
;;;;      y                    `+                    -`
;;;;   ``` ` ` `            ````   ``            ``` ` ` `
;;;;   `` ` ` `             ``````   `            ``` ` ` `
;;;; -----------------------------------------------------------------

;;;; HACK: Abstract more (also in tests). (mae 15-10-12)

;;;; HACK: Move general graph stuff to a own lib in order to enable a
;;;;       better orthogonality. (mae 15-10-19)

;;;; HACK: May be implement other component funcs (e.g. get-comp).
;;;;       (mae 15-10-19)

;;;; TODO: Add factory functions for creating components. (mae 15-10-30)

(def not-contains? (complement contains?))

(defn in? [xs e]
  (some #(= e %) xs))

;;; Core graph representation

(defn new-graph
  "Creates a new graph."
  []
  {:d {} :p {}})

(defn gd
  "Returns the 'defintion' of the graph g."
  [g]
  (:d g))

(defn gp
  "Returns the 'properties' of the graph g."
  [g]
  (:p g))

(defn add-node
  "Adds a node (any key) to the graph g."
  [g n]
  {:pre [(not-contains? (gd g) n)]}
  (update-in g [:d] assoc n []))

(defn remove-node-source
  [g n]
  {:pre [(contains? (gd g) n)]}
  (update-in g [:d] dissoc n))

(defn- update-connections
  [g f]
  (update-in g [:d] (partial reduce-kv f {})))

(defn  remove-node-destination
  [g n]
  (update-connections g (fn [m k v] (assoc m k (remove #(= % n) v)))))

(defn remove-node
  "Removes a node (any key) from the graph g."
  [g n]
  (-> g (remove-node-source n) (remove-node-destination n)))

(defn rename-node-source
  [g from to]
  {:pre [(and (contains? (gd g) from) (not-contains? (gd g) to))]}
  (update-in g [:d] #(set/rename-keys % {from to})))

(defn rename-node-destination
  [g from to]
  (update-connections g (fn [m k v] (assoc m k (replace {from to} v)))))

(defn rename-node
  "Renames a node in the graph g from a key to another."
  [g from to]
  (-> g (rename-node-source from to) (rename-node-destination from to)))

(defn add-edge
  "Adds an edge from node/key a to node/key b to the graph g."
  [g from to]
  {:pre [(every? (partial contains? (gd g)) [from to])]}
  (update-in g [:d from] conj to))

(defn remove-edge
  "Removes the edge form node/key a to node/key b from the graph g."
  [g from to]
  {:pre [(contains? (gd g) from)]}
  (update-in g [:d from] (partial remove #(= % to))))

(defn add-property
  "Adds a property (any data e.g. a map) to an existing node."
  [g n p]
  {:pre [(contains? (gd g) n)]}
  (update-in g [:p] assoc n p))

(defn remove-property
  "Removes a property (any data e.g. a map) from an existing node."
  [g n]
  (update-in g [:p] dissoc n))

(defn get-nodes
  "Returns all the nodes of a graph."
  [g]
  (keys (gd g)))

(defn get-outgoing
  "Returns the end-nodes of the outgoing edges of node n."
  [g n]
  {:pre [(contains? (gd g) n)]}
  (get (gd g) n))

(defn get-incoming
  "Returns the start-nodes of the incoming edges of node n."
  [g n]
  (for [[k v] (gd g)
        :when (in? v n)]
    k))

(defn spout?
  "Returns true if the node n is a spout in the graph g and does not
  have any incoming edges, respectively."
  [g n]
  (empty? (get-incoming g n)))

(defn sink?
  "Return true if the node n is a sink in the graph g and does not
  have any outgoing edges, respectively."
  [g n]
  (empty? (get-outgoing g n)))

(defn get-spouts
  "Returns all the nodes that are spouts."
  [g]
  (for [[k v] (gd g)
        :when (spout? g k)]
    k))

(defn get-sinks
  "Returns all the nodes that are sinks."
  [g]
  (for [[k v] (gd g)
        :when (sink? g k)]
    k))

;;; Graphite specific stuff

(defn- new-component-property
  [component config]
  {:component component
   :config    config})

(defn add-component
  "Adds a component with id n and optionally its configuration to the
  graph g."
  ([g n component]
   (add-component g n component nil))
  ([g n component config]
   (-> g
       (add-node n)
       (add-property n (new-component-property component config)))))

(defn remove-component
  "Removes the component with id n from the graph g."
  [g n]
  (-> g (remove-node n) (remove-property n)))

;; DSL

(defn tripels [s]
  (partition 3 s))

(defn tripels->components
  [tripels]
  (letfn [(->c [v]
          {:n (first v)
           :component (second v)
           :config (nth v 2 nil)})]
    (for [t tripels]
      [(->c (first t)) (->c (nth t 2))])))

(defn build-graph
  [components]
  (letfn [(+component [g c]
            (if (and (not-contains? (gd g) (:n c))
                       (not (nil? (:component c))))
              (add-component g (:n c) (:component c) (:config c))
              g))
          (+edge [g c1 c2]
            (add-edge g (:n c1) (:n c2)))]
    (reduce (fn [g v]
              (let [[c1 c2] v]
                (-> g (+component c1) (+component c2) (+edge c1 c2))))
            (new-graph)
            components)))

(defmacro defgraph
  [s]
  (build-graph (tripels->components (tripels s))))
